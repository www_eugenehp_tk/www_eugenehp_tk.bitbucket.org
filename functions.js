      function loadMapui() {
      	var script = document.createElement('script');
        script.src = 'map-ui.js';
        var body = document.getElementsByTagName('body')[0];
        body.appendChild(script);
      }
      
      function constructNewCoordinates(polygon, name) {
      	var newCoordinates = [];
	if (polygon) {  	
		var coordinates = polygon['coordinates'][0];
		for (var i in coordinates) {
		  newCoordinates.push(
		      new google.maps.LatLng(coordinates[i][1], coordinates[i][0]));
		}
		return newCoordinates;
        } else {
        	console.log('DATABASE ERROR IN POLYGON ' + name);
        	newCoordinates.push(
              new google.maps.LatLng(0, 0));
        	return newCoordinates;
        }
      }
      
     /* function polygonClicklistener(polygonsArray) {
      	    for(var i = 0; i < polygonsArray.length; i++) {
      	    	for(var j = 0; j < polygonsArray[i].length; j ++) {
      	    	  console.log(polygonsArray[i][j]);
		  google.maps.event.addListener(polygonsArray[i][j], 'click', function() {
		    var oldOpacity = Number(this.fillOpacity);
		    var newOpacity = Number;
		      if (oldOpacity === this.hoverOpacity) {
		      	      newOpacity= this.selectOpacity;
		      	      polygonClearlisteners(polygonsArray[i]);
			  } if (oldOpacity === this.selectOpacity) {
			      newOpacity= this.hoverOpacity;
			      assemListeners(assemarray);
			  } if (oldOpacity === this.normOpacity) {
			      console.log('do nothing');
			      newOpacity= this.normOpacity;
			  }
	            this.setOptions({fillOpacity:newOpacity});
		  });
		}
      	    }
      }
      
      function polygonClearlisteners(assemarray) {
      	      console.log(assemarray);
      	      for (var a in assemarray) {
      	      	google.maps.event.clearListeners(assemarray[a], 'mouseover');
	        google.maps.event.clearListeners(assemarray[a], 'mouseout');
      	      }
      } */
