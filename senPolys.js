      function drawsen(data) {
      	console.log('senate loaded');
      	document.getElementById('seninfo').innerHTML = '';
      	var rows = data['rows'];
        for (var i in rows) {
          if (rows[i][0] != '') {
            var newCoordinates = [];
            var name = rows[i][1];
            var geometries = rows[i][0]['geometries'];
            if (geometries) {
              for (var j in geometries) {
                newCoordinates.push(constructNewCoordinates(geometries[j]));
              }
            } else {
              newCoordinates = constructNewCoordinates(rows[i][0]['geometry']);
            }
            if (rows[i][2] == 'senate'){
            	    var normOpacity = 0.2;
            	    var Senate = new google.maps.Polygon({
            	      description: 'Senate Polygon',
            	      district: rows[i][1],
            	      paths: newCoordinates,
		      strokeColor: "#eb823c",
		      strokeOpacity: 1,
		      strokeWeight: 3,
		      fillColor: "#FF0000",
		      normOpacity: normOpacity,
		      fillOpacity: normOpacity,
		      hoverOpacity: 0.8,
		      selectOpacity: 1,
		      zIndex: 2
            	    });
            	    senarray.push(Senate);
            }
          }
        }
        if (senarray != ''){
        	senListeners(senarray);
        	polygonsArray.push(senarray);
        }
        
        checkSenListener(senarray);
        senClicklistener(senarray);
      }
      
      function senZindex(sorted) {
      	      var partsArray = sorted.split('&');
      	      console.log(partsArray);
      	      for (var s in senarray) {
		      senarray[s].setMap(null);      	      	      
		      if (partsArray[0] == 'layer=senate') {
				      senarray[s].zIndex=4;
		      }
		      if (partsArray[1] == 'layer=senate') {
				      senarray[s].zIndex=3;
		      }
		      if (partsArray[2] == 'layer=senate') {
				      senarray[s].zIndex=2;
		      }
		      if (partsArray[3] == 'layer=senate') {
				      senarray[s].zIndex=1;
		      }
		      if (senate.checked) {
		      	      senarray[s].setMap(map);
		      }
	      }   	      
      }
      
      function senListeners(senarray) {
      	      for (var s in senarray) {     	      	    
      	          google.maps.event.addListener(senarray[s], 'mouseover', function() {
		    this.setOptions({fillOpacity: this.hoverOpacity});
		    document.getElementById('seninfo').innerHTML = this.district;
		  });
		  google.maps.event.addListener(senarray[s], 'mouseout', function() {
		    this.setOptions({fillOpacity: this.normOpacity});
		    document.getElementById('seninfo').innerHTML = '';
		  });
	      }
      }
      
      function senClearlisteners(senarray) {
      	      for (var s in senarray) {
      	      	google.maps.event.clearListeners(senarray[s], 'mouseover');
	        google.maps.event.clearListeners(senarray[s], 'mouseout');
      	      }
      }
      
      function senClicklistener(senarray) {
      	      for (var s in senarray) {
		  google.maps.event.addListener(senarray[s], 'click', function() {
		    var oldOpacity = Number(this.fillOpacity);
		    var newOpacity = Number;
		      if (oldOpacity === this.hoverOpacity) {
		      	      newOpacity= this.selectOpacity;
		      	      senClearlisteners(senarray);
			  } if (oldOpacity === this.selectOpacity) {
			      newOpacity= this.hoverOpacity;
			      senListeners(senarray);
			  } if (oldOpacity === this.normOpacity) {
			      console.log('do nothing');
			      newOpacity= this.normOpacity;
			  }
	            this.setOptions({fillOpacity:newOpacity});
		  });
      	      }
      }
      
      function checkSenListener(senarray) {
      	      google.maps.event.addDomListener(document.getElementById('senate'), 'click', function() {
			  if (senate.checked) {
			    for (var s in senarray) {
			      senarray[s].setMap(map);
			    }
			    document.getElementById('senlabel').innerHTML= 'Senate District:';
			  }
			  else if (!senate.checked) {
			    for (var s in senarray) {
			      senarray[s].fillOpacity=0.2;
			      senListeners(senarray);
			      senarray[s].setMap(null);
			     }
			     document.getElementById('seninfo').innerHTML = '';
			     document.getElementById('senlabel').innerHTML= 'Senate District';
			  }
	      });
      }
