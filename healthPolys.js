      function drawhealth(data) {
      	console.log('health loaded',data);
        document.getElementById('healthinfo').innerHTML = '';
      	var rows = data['rows'];
        for (var i in rows) {
          if (rows[i][0] != '') {
            var newCoordinates = [];
            var name = rows[i][1];
            var geometries = rows[i][0]['geometries'];
            if (geometries) {
              for (var j in geometries) {
                newCoordinates.push(constructNewCoordinates(geometries[j]));
              }
            } else {
              newCoordinates = constructNewCoordinates(rows[i][0]['geometry']);
            }
            if (rows[i][2] == 'HealthCare' || rows[i][2] == 'PoliceProtection_Ambulance' || rows[i][2] == 'Not CSDA Member - No IMIS_ID') {
            	    var normOpacity = 0.2;
            	    var address = rows[i][8];
            	    if (name == 'Cloverdale Health Care District') {
            	    	   var zIndex = 7;
            	    } else {
            	    	    var zIndex = 6;
            	    }
            	    address = address.replace(',', '<br/>');
            	    var Healthcare = new google.maps.Polygon({
		      description: "Healthcare Polygon",
		      district: rows[i][1],
		      url: rows[i][3],
		      county: rows[i][4],
		      zipcode: rows[i][5],
		      senate: rows[i][6],
		      assembly: rows[i][7],
		      address: address,
		      latitude: rows[i][9],
		      longitude: rows[i][10],
		      paths: newCoordinates,
		      strokeColor: "#333333",
		      strokeOpacity: 1,
		      strokeWeight: 3,
		      fillColor: "#6fa8dc",
		      normOpacity: normOpacity,
		      fillOpacity: normOpacity,
		      hoverOpacity: 0.8,
		      selectOpacity: 1,
		      zIndex: zIndex
		    });
		    healtharray.push(Healthcare);
            }
          }
        }
        if (healtharray != '') {
        	healthListeners(healtharray);
        	//polygonsArray.push(healtharray);
        }
        
        createMarkers(healtharray);
        clickListener(healtharray);
        checkListener(healtharray);
      }
      
      function createMarkers(healtharray) {
      	      var addressMarkers = [];
      	      for (var h in healtharray) {
      	        //console.log(healtharray[h].latitude+", "+healtharray[h].longitude);
      	        var myLatlng = new google.maps.LatLng(healtharray[h].latitude,healtharray[h].longitude);
      	        var addresslayer = new google.maps.Marker({
      	          position: myLatlng,
      	          title: healtharray[h].district,
      	          zIndex: 5  
      	        });
      	        addressMarkers.push(addresslayer);
      	      }
      	      addressListener(addressMarkers);
      }
      
      function addressListener(addressMarkers) {
      	  google.maps.event.addDomListener(document.getElementById('address'), 'click', function() {
      	    if (address.checked) {
      	      for (var a in addressMarkers) {
      	        addressMarkers[a].setMap(map);
      	      }
	    } else if (!address.checked) {
	        for (var a in addressMarkers) {
		    addressMarkers[a].setMap(null);
	    	}
	    }
	  });
      }
      
      function clearListeners(healtharray) {
      	      for (var h in healtharray) {
      	      	google.maps.event.clearListeners(healtharray[h], 'mouseover');
	        google.maps.event.clearListeners(healtharray[h], 'mouseout');
      	      }
      }
      
      function clearInformation() {
      	      document.getElementById('hcdlabel').innerHTML = 'Healthcare Districts';
      	      document.getElementById('h_name').innerHTML = '';
      	      document.getElementById('h_county').innerHTML = '';
      	      document.getElementById('h_zipcode').innerHTML = '';
      	      document.getElementById('h_senate').innerHTML = '';
      	      document.getElementById('h_assembly').innerHTML = '';
      	      document.getElementById('h_address').innerHTML = '';
      }
      
      function healthListeners(healtharray) {
      	      for (var h in healtharray) {
      	      	  healtharray[h].setMap(map);    
      	          google.maps.event.addListener(healtharray[h], 'mouseover', function() {
		    this.setOptions({fillOpacity: this.hoverOpacity});
		    document.getElementById('hcdlabel').innerHTML = 'Healthcare District:';
		    document.getElementById('h_county').innerHTML = this.county;
		    document.getElementById('h_zipcode').innerHTML = this.zipcode;
		    document.getElementById('h_senate').innerHTML = this.senate;
		    document.getElementById('h_assembly').innerHTML = this.assembly;
		    document.getElementById('h_address').innerHTML = this.address;
		    if (this.url != ''){
			  document.getElementById('h_name').innerHTML = "<a href='" + this.url + "' target='_blank'>" + this.district + "</a>";
		    } else {
		  	  document.getElementById('h_name').innerHTML = this.district;
		    }
		  });
		  google.maps.event.addListener(healtharray[h], 'mouseout', function() {
		    this.setOptions({fillOpacity: this.normOpacity});
		    clearInformation();
		  });
	      }
      }

      function clickListener(healtharray) {
      	      for (var h in healtharray) {
		  google.maps.event.addListener(healtharray[h], 'click', function() {
		    var oldOpacity = Number(this.fillOpacity);
		    var newOpacity = Number;
		      if (oldOpacity === this.hoverOpacity) {
		      	      newOpacity= this.selectOpacity;
		      	      clearListeners(healtharray);
			  } if (oldOpacity === this.selectOpacity) {
			      newOpacity= this.hoverOpacity;
			      healthListeners(healtharray);
			  } if (oldOpacity === this.normOpacity) {
			      console.log('do nothing');
			      newOpacity= this.normOpacity;
			  }
	            this.setOptions({fillOpacity:newOpacity});
		  });
      	      }
      }
      
      /*function setNormalopacity() {
      	      for (var h in healtharray) {
      	      	      healtharray[h].setOptions({fillOpacity:healtharray[h].normOpacity});
      	      }
      }*/
      
      function checkListener(healtharray) {
      	      google.maps.event.addDomListener(document.getElementById('health'), 'click', function() {
			  if (health.checked) {
			    for (var h in healtharray) {
			      healtharray[h].setMap(map);
			    }
			  }
			  else if (!health.checked) {
			    for (var h in healtharray) {
			      healtharray[h].fillOpacity=0.2;
			      healtharray[h].setMap(null);
			    }
			    //setNormalopacity();
			    clearListeners();
			    clearInformation();
			  }
	      });
      }
      
