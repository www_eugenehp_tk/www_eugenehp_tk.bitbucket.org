function drawassem(data) {
	//TODO: "Response size is larger than 10 MB.  Please use media download v2."
  console.log('assembly loaded',data);
  document.getElementById('asseminfo').innerHTML = '';
  var rows = data['rows'];
        for (var i in rows) {
          if (rows[i][0] != '') {
            var newCoordinates = [];
            var name = rows[i][1];
            var geometries = rows[i][0]['geometries'];
            if (geometries) {
              for (var j in geometries) {
                newCoordinates.push(constructNewCoordinates(geometries[j]));
              }
            } else {
              newCoordinates = constructNewCoordinates(rows[i][0]['geometry']);
            }
            if (rows[i][2] == 1){
            	    var normOpacity = 0.2;
            	    var Assembly = new google.maps.Polygon({
            	      description: 'Assembly Polygon',
            	      prefix: 'assem',
            	      district: rows[i][1],
            	      paths: newCoordinates,
		      strokeColor: "#8ec214",
		      strokeOpacity: 1,
		      strokeWeight: 3,
		      fillColor: "#4AA02C",
		      normOpacity: normOpacity,
		      fillOpacity: normOpacity,
		      hoverOpacity: 0.8,
		      selectOpacity: 1,
		      zIndex: 3
            	    });
            	    assemarray.push(Assembly);
            }
          }
        }
        if (assemarray != ''){
		assemListeners(assemarray);
		polygonsArray.push(assemarray);
        }
        checkAssemListener(assemarray);
        assemClicklistener(assemarray);
        loadMapui();
      }
      
      function loadMapui() {
      	      var script = document.createElement('script');
      	      script.src = 'map-ui.js';
      	      var body = document.getElementsByTagName('body')[0];
      	      body.appendChild(script);
      }
      
      function assemZindex(sorted) {
      	      var partsArray = sorted.split('&');
      	      console.log(partsArray);
      	      for (var a in assemarray) {
		      assemarray[a].setMap(null);
		      if (partsArray[0] == 'layer=assembly') {
				      assemarray[a].zIndex=4;
			      }
		      if (partsArray[1] == 'layer=assembly') {
	
				      assemarray[a].zIndex=3;
			      }
		      if (partsArray[2] == 'layer=assembly') {
				      assemarray[a].zIndex=2;
			      }
		      if (partsArray[3] == 'layer=assembly') {
				      assemarray[a].zIndex=1;
			      }
		      if (assembly.checked) {
		      	      assemarray[a].setMap(map);
		      }
	      }
      }

      function assemListeners(assemarray) {
      	      for (var a in assemarray) {     	      	    
      	          google.maps.event.addListener(assemarray[a], 'mouseover', function() {
		    this.setOptions({fillOpacity: this.hoverOpacity});
		    document.getElementById('asseminfo').innerHTML = this.district;
		  });
		  google.maps.event.addListener(assemarray[a], 'mouseout', function() {
		    this.setOptions({fillOpacity: this.normOpacity});
		    document.getElementById('asseminfo').innerHTML = '';
		  });
	      }
      }
      
      function assemClearlisteners(assemarray) {
      	      for (var a in assemarray) {
      	      	google.maps.event.clearListeners(assemarray[a], 'mouseover');
	        google.maps.event.clearListeners(assemarray[a], 'mouseout');
      	      }
      }
      
      function assemClicklistener(assemarray) {
      	      for (var a in assemarray) {
		  google.maps.event.addListener(assemarray[a], 'click', function() {
		    var oldOpacity = Number(this.fillOpacity);
		    var newOpacity = Number;
		      if (oldOpacity === this.hoverOpacity) {
		      	      newOpacity= this.selectOpacity;
		      	      assemClearlisteners(assemarray);
			  } if (oldOpacity === this.selectOpacity) {
			      newOpacity= this.hoverOpacity;
			      assemListeners(assemarray);
			  } if (oldOpacity === this.normOpacity) {
			      console.log('do nothing');
			      newOpacity= this.normOpacity;
			  }
	            this.setOptions({fillOpacity:newOpacity});
		  });
      	      }
      }

      function checkAssemListener(assemarray) {
      	      google.maps.event.addDomListener(document.getElementById('assembly'), 'click', function() {
			  if (assembly.checked) {
			    for (var a in assemarray) {
			      assemarray[a].setMap(map);
			    }
			    document.getElementById('assemlabel').innerHTML = 'Assembly District:';
			  }
			  else if (!assembly.checked) {
			    for (var a in assemarray) {
			      assemarray[a].fillOpacity=0.2;
			      assemListeners(assemarray);
			      assemarray[a].setMap(null);
			    }
			    document.getElementById('asseminfo').innerHTML = '';
			    document.getElementById('assemlabel').innerHTML = 'Assembly District';
			  }
	      });
      }
