      function drawcount(data) {
      	console.log('county loaded',data);
      	document.getElementById('countinfo').innerHTML = '';
      	var rows = data['rows'];
        for (var i in rows) {
          if (rows[i][0] != '') {
            var newCoordinates = [];
            var name = rows[i][1];
            var geometries = rows[i][0]['geometries'];
            if (geometries) {
              for (var j in geometries) {
                newCoordinates.push(constructNewCoordinates(geometries[j]));
              }
            } else {
              newCoordinates = constructNewCoordinates(rows[i][0]['geometry']);
            }
            if (rows[i][2] == 'county'){
            	    var normOpacity = 0.2;
            	    var County = new google.maps.Polygon({
            	      description: 'County Polygon',
            	      district: rows[i][1],
            	      paths: newCoordinates,
		      strokeColor: "#94a5e7",
		      strokeOpacity: 1,
		      strokeWeight: 3,
		      fillColor: "#6c86ea",
		      normOpacity: normOpacity,
		      fillOpacity: normOpacity,
		      hoverOpacity: 0.8,
		      selectOpacity: 1,
		      zIndex: 4
            	    });
            	    countarray.push(County);
            }
          }
        }
        if (countarray != ''){
		countListeners(countarray);
		polygonsArray.push(countarray);
        }
        checkCountListener(countarray);
        countClicklistener(countarray);
      }
      
      function countZindex(sorted) {
      	      var partsArray = sorted.split('&');
      	      console.log(partsArray);
      	      for (var c in countarray) {
      	      	      countarray[c].setMap(null);
		      if (partsArray[0] == 'layer=county') {    
				      countarray[c].zIndex=4;
		      }
		      if (partsArray[1] == 'layer=county') {
				      countarray[c].zIndex=3;
		      }
		      if (partsArray[2] == 'layer=county') {
				      countarray[c].zIndex=2;
		      }
		      if (partsArray[3] == 'layer=county') {
				      countarray[c].zIndex=1;
		      }
		      if (county.checked) {
		      	      countarray[c].setMap(map);
		      }
	      } 	      
      }
      
      function countListeners(countarray) {
      	      for (var y in countarray) {     	      	    
      	          google.maps.event.addListener(countarray[y], 'mouseover', function() {
		    this.setOptions({fillOpacity: this.hoverOpacity});
		    document.getElementById('countinfo').innerHTML = this.district;
		  });
		  google.maps.event.addListener(countarray[y], 'mouseout', function() {
		    this.setOptions({fillOpacity: this.normOpacity});
		    document.getElementById('countinfo').innerHTML = '';
		  });
	      }
      }
      
      function countClearlisteners(countarray) {
      	      for (var c in countarray) {
      	      	google.maps.event.clearListeners(countarray[c], 'mouseover');
	        google.maps.event.clearListeners(countarray[c], 'mouseout');
      	      }
      }
      
      function countClicklistener(countarray) {
      	      for (var c in countarray) {
		  google.maps.event.addListener(countarray[c], 'click', function() {
		    var oldOpacity = Number(this.fillOpacity);
		    var newOpacity = Number;
		      if (oldOpacity === this.hoverOpacity) {
		      	      newOpacity= this.selectOpacity;
		      	      countClearlisteners(countarray);
			  } if (oldOpacity === this.selectOpacity) {
			      newOpacity= this.hoverOpacity;
			      countListeners(countarray);
			  } if (oldOpacity === this.normOpacity) {
			      console.log('do nothing');
			      newOpacity= this.normOpacity;
			  }
	            this.setOptions({fillOpacity:newOpacity});
		  });
      	      }
      }
      
      function checkCountListener(countarray) {
      	      google.maps.event.addDomListener(document.getElementById('county'), 'click', function() {
			  if (county.checked) {
			    for (var y in countarray) {
			      countarray[y].setMap(map);
			    }
			    document.getElementById('countylabel').innerHTML= 'County:';
			  }
			  else if (!county.checked) {
			    for (var y in countarray) {
			      countarray[y].fillOpacity=0.2;
			      countListeners(countarray);
			      countarray[y].setMap(null);
			    }
			    document.getElementById('countinfo').innerHTML = '';
			    document.getElementById('countylabel').innerHTML= 'County';
			  }
	      });
      }
      

