console.log('map ui loaded');

  $(function() {
    $( "#layers" ).sortable({
      revert: 100,
      update: function( event, ui ) {
      		var sorted = $( "#layers" ).sortable( "serialize", { key: "layer" } );
      		console.log(sorted);
      		countZindex(sorted);
      		assemZindex(sorted);
      		senZindex(sorted);
      		congZindex(sorted);
      }
    });
    $( "ul, li" ).disableSelection();
  });
