      function drawcong(data) {
      	console.log('congressional loaded',data);    
        document.getElementById('conginfo').innerHTML = '';
      	var rows = data['rows'];
        for (var i in rows) {
          if (rows[i][0] != '') {
            var newCoordinates = [];
            var name = rows[i][1];
            var geometries = rows[i][0]['geometries'];
            if (geometries) {
              for (var j in geometries) {
                newCoordinates.push(constructNewCoordinates(geometries[j]));
              }
            } else {
              newCoordinates = constructNewCoordinates(rows[i][0]['geometry']);
            }
            if (rows[i][2] == 'congressional'){
            	    var normOpacity = 0.2;
            	    var Congressional = new google.maps.Polygon({
            	      description: 'Congressional Polygon',
            	      district: rows[i][1],
            	      paths: newCoordinates,
		      strokeColor: "#333333",
		      strokeOpacity: 1,
		      strokeWeight: 3,
		      fillColor: "#ffa500",
		      normOpacity: normOpacity,
		      fillOpacity: normOpacity,
		      hoverOpacity: 0.8,
		      selectOpacity: 1,
		      zIndex: 1
            	    });
            	    congarray.push(Congressional);
            }
          }
        }
        if (congarray != ''){
		congListeners(congarray);
		polygonsArray.push(congarray);
        }
        checkCongListener(congarray);
        congClicklistener(congarray);
      }
      
      function congZindex(sorted) {
      	      var partsArray = sorted.split('&');
      	      console.log(partsArray);
      	      for (var c in congarray) {
		      congarray[c].setMap(null);
		      if (partsArray[0] == 'layer=congressional') {
				      congarray[c].zIndex=4;
		      }
		      if (partsArray[1] == 'layer=congressional') {
				      congarray[c].zIndex=3;
		      }
		      if (partsArray[2] == 'layer=congressional') {
				      congarray[c].zIndex=2;
		      }
		      if (partsArray[3] == 'layer=congressional') {
				      congarray[c].zIndex=1;
		      }
		      if (congressional.checked) {
		      	      congarray[c].setMap(map);
		      }
      	      }
      }
      
      function congListeners(congarray) {
      	      for (var c in congarray) {     	      	    
      	          google.maps.event.addListener(congarray[c], 'mouseover', function() {
		    this.setOptions({fillOpacity: this.hoverOpacity});
		    document.getElementById('conginfo').innerHTML = this.district;
		  });
		  google.maps.event.addListener(congarray[c], 'mouseout', function() {
		    this.setOptions({fillOpacity: this.normOpacity});
		    document.getElementById('conginfo').innerHTML = '';
		  });
	      }
      }
      
      function congClearlisteners(congarray) {
      	      for (var c in congarray) {
      	      	google.maps.event.clearListeners(congarray[c], 'mouseover');
	        google.maps.event.clearListeners(congarray[c], 'mouseout');
      	      }
      }
      
      function congClicklistener(congarray) {
      	      for (var c in congarray) {
		  google.maps.event.addListener(congarray[c], 'click', function() {
		    var oldOpacity = Number(this.fillOpacity);
		    var newOpacity = Number;
		      if (oldOpacity === this.hoverOpacity) {
		      	      newOpacity= this.selectOpacity;
		      	      congClearlisteners(congarray);
			  } if (oldOpacity === this.selectOpacity) {
			      newOpacity= this.hoverOpacity;
			      congListeners(congarray);
			  } if (oldOpacity === this.normOpacity) {
			      console.log('do nothing');
			      newOpacity= this.normOpacity;
			  }
	            this.setOptions({fillOpacity:newOpacity});
		  });
      	      }
      }
      
      function checkCongListener(congarray) {
      	      google.maps.event.addDomListener(document.getElementById('congressional'), 'click', function() {
			  if (congressional.checked) {
			    for (var c in congarray) {
			      congarray[c].setMap(map);
			    }
			    document.getElementById('conglabel').innerHTML= 'Congressional District:';
			  }
			  else if (!congressional.checked) {
			    for (var c in congarray) {
			      congarray[c].fillOpacity=0.2;
			      congListeners(congarray);
			      congarray[c].setMap(null);
			    }
			    document.getElementById('conginfo').innerHTML = '';
			    document.getElementById('conglabel').innerHTML= 'Congressional District';
			  }
	      });
      }      
